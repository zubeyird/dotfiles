#dotfiles

Here is my Archbased distro I3 tiling manager dotfiles. In several vays optimized for two computer. 

- [x] Trackpad's moving and scrolling speed adjusted for T440P.
- [x] External mouse settings adjusted.

## Terminal emulators

### Allacritty

- [x] Setted default as zsh
- [x] Setted as main shell

### Xurvt

- [x] Zsh shell setted
- [x] External alias folder setted
- [x] Anaconda added

## Vim config

It is personalized for C, CPP, Python and Bash languages.

## License
No license requiered

