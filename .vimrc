"""
" Author:  Zubeyir Durgut
" Date:     2023 03 13
" Aim:      Specialized for writing C and C++ code.
"
"""
"First time plug installation
"curl -fLo ~/.vim/autoload/plug.vim --create-dirs \    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
"Update
"PlugUpdate
"""
"execute pathogen#infect()
syntax on

set nu
set nocompatible
filetype plugin off

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set wrap                                    "nowrap and wrap status tab line appears and disappears
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir/
set undofile
set incsearch
"set hls is                                  "It remain highlighted even jump other
set ic
set termguicolors

set autoread                                "Read automatically out edited current buffer
set spell spelllang=en_us,es_es,tr
set relativenumber
set tags=./tags;/
set signcolumn=no                           "
set wildmenu                                "opens tab suggestion menu while cli active
set term=xterm-256color
set t_ut=

"keyset

map <C-J> :bnext<CR> " switching to next buffer 
map <C-K> :bprev<CR> " switching to prev buffer 
map <C-L> :tabn<CR>
map <C-H> :tabp<CR>
map <c-n> :bn<cr>
map <c-p> :bp<cr>

"Terminal 
nnoremap <F2> :vert new \| Explore<CR>
nnoremap <F3> :botright vertical terminal<CR>
nnoremap <F4> :tab term<CR>

" vimgrep command
command! -nargs=1 -complete=file -bar -bang -range -buffer VG execute 'vimgrep' <q-args> expand('%:h') . '/*'


" register key assignment


"debuggung and running
"xclang linter
autocmd filetype c nnoremap <F5> :w<bar>vertical botrigh terminal ++shell gcc -g -Wextra -Wall -Wextra -Wwrite-strings -Wno-parentheses -Wpedantic -Warray-bounds -Wconversion  -Wstrict-prototypes -fanalyzer -lm %:p -o %:p:r && %:p:r<CR>
"autocmd filetype c nnoremap <F5> :w<bar>vertical botrigh terminal ++shell gcc-11 -g -Wextra -lm %:p -o %:p:r && %:p:r<CR>
autocmd filetype cpp nnoremap <F5> :w<bar>vertical botrigh terminal ++shell g++ -g -O2 -Wall -std=c++11 -lm %:p -o %:p:r && %:p:r<CR>
nnoremap <F6><bar> ++shell clang-format -i -style=webkit %:p "clang -c -Xclang -ast-dump %:p<CR>

autocmd filetype c nnoremap <F6> :w<bar>vertical botrigh terminal ++shell gdb -g -Wextra -Wall -Wextra -Wwrite-strings -Wno-parentheses -Wpedantic -Warray-bounds -Wconversion  -Wstrict-prototypes -fanalyzer -lm %:p -o %:p:r && %:p:r<CR>
" Filetype specific embeding info with keybinding.
" ToDo: Update should be depend on demand. 
autocmd FileType python,c,cpp,rs nnoremap <F10> :call GetInfo()<CR>

function! GetInfo()
  let l:writer = 'Zubeyir Durgut'
  let l:date = strftime("%Y-%m-%d %H:%M:%S")
  let l:license = "GPL-3"
  let l:comment_char = '"""'
  let l:file_name = expand('%:t')
  let l:project_name = fnamemodify(getcwd(), ':t')
  let l:version = 1
  let l:version_file = findfile('VERSION', '.;')
  if l:version_file !=# ''
    let l:version = trim(readfile(l:version_file)[0])
  endif

  let l:contact = 'zubeyird@ktu.edu.tr'
  let l:aim = 'This code does:' 
  let l:todo= '***' 
  let l:changes = ''
  let l:comment = l:comment_char . "\n"
  \ . " Project : " . l:project_name . "\n"
  \ . " File    : " . l:file_name . "\n"
  \ . " Author  : " . l:writer . "\n"
  \ . " Date    : " . l:date . "\n"
  \ . " License : " . l:license . "\n"
  \ . " Version : " . l:version . "\n"
  \ . " Contact : " . l:contact . "\n"
  \ . " Aim     : " . l:aim. "\n"
  \ . " ToDo    :\n \t \t"  . l:todo. "\n"
  \ . " Changes : " . l:changes . "\n"
  \ . l:comment_char

  let l:lines = split(l:comment, "\n")
  call setline(1, l:lines)
endfunction


call plug#begin('~/.vim/plugged')

Plug 'rust-lang/rust.vim'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'leafgarland/typescript-vim'
Plug 'tpope/vim-fugitive'
Plug 'mtdl9/vim-log-highlighting'

"Editing plugins
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'


"theme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'nanotech/jellybeans.vim'
Plug 'cornet/ccze'

" vim-gitgutter plugin
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-git'
Plug 'itchyny/vim-gitbranch'

"Language server protocol (lsp)
" prerequistes
" nodejs npm 
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-python/python-syntax', { 'for': 'python'}
Plug 'dense-analysis/ale'

"markdown
Plug 'vimwiki/vimwiki'
"Plug 'instant-markdown/vim-instant-markdown', {'for': 'markdown', 'do': 'yarn install'}
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'ap/vim-css-color'                                 " It enables CSS colour texts'
Plug 'junegunn/goyo.vim'


"Documentation 
"Plug 'kana/vim-python-doc'

" Color Schemes
Plug 'altercation/vim-colors-solarized' 
Plug 'crusoexia/vim-monokai'            
Plug 'gruvbox-community/gruvbox'
Plug 'tomasr/molokai'                   


call plug#end()

"theme section
"
let g:my_color_schemes = ['solarized', 'monokai', 'gruvbox', 'molokai']

syntax enable

"vertical column
set colorcolumn=80
set guifont=LiberationMono\ 18

"colorscheme GruvboxAqua
"gruvbox
let g:solarized_termcolors=256
colorscheme gruvbox
set background=dark    " Setting dark mode 
"set background=light   " Setting light mode

"  "background" : "#282828",
"  "black" : "#282828",
"  "blue" : "#458588",
"  "brightBlack" : "#928374",
"  "brightBlue" : "#83A598",
"  "brightCyan" : "#8EC07C",
"  "brightGreen" : "#B8BB26",
"  "brightPurple" : "#D3869B",
"  "brightRed" : "#FB4934",
"  "brightWhite" : "#EBDBB2",
"  "brightYellow" : "#FABD2F",
"  "cyan" : "#689D6A",
"  "foreground" : "#EBDBB2",
"  "green" : "#98971A",
"  "name" : "Gruvbox Dark",
"  "purple" : "#B16286",
"  "red" : "#CC241D",
"  "white" : "#A89984",
"  "yellow" : "#D79921"
let g:gruvbox_contrast_dark = 'light'
"highlight Normal "guibg=#7c6f64 guifg=#7c6f64
"let g:gruvbox_colors =  'bg4': ['#7c6f64', 0] }
"let g:gruvbox_background='bg4'
"let g:gruvbox_contrast_dark='hard'
"let g:gruvbox_invert_tabline=1
"let g:gruvbox_transparent_bg=1
"let s:gruvbox_orginal_palette= ['#7c6f64', 243]     " 124-111-100
"let g:gruvbox_orginal_palette='bg4'
"let g:gruvbox_meterial_better_performance = 1
"

" airline
let g:airline_theme='gruvbox'
let g:airline_powerline_fonts = 1

"let g:airline_symbols.branch = '⭠'
"let g:airline_symbols.filetype = '▶'
"let g:airline_symbols.readonly = '🔒'
"let g:airline_symbols.dirty = '✱'
"let g:airline_symbols.left_sep = '❮'
"let g:airline_symbols.right_sep = '❯'

" Cursor  color
"highlight Cursor guifg=black
"highlight iCursor guifg=white guibg=steelblue
"set guicursor=n-v-c:block-Cursor
"set guicursor+=i:ver100-iCursor
"set guicursor+=n-v-c:blinkon0
"set guicursor+=i:blinkwait10

"lua settings

"let mapleader = ","
""nnoremap <leader>tt <cmd>Telescope find_files<cr>
"nnoremap <leader>ps :lua require('telescope.builtin').grep_string( search = vim.fn.input("Grep For > ")})<CR>
"nnoremap <C-w> <ALT+w>

" Example status line format: [branch-name +n -m n]
set statusline+=%fugitive#statusline()}

"vim-lsp configuration 
let g:completition_mathing_strategy_list = ['exact', 'substring', 'fuzzy']
if executable('pyls')
    " pip install python-language-server
    au User lsp_setup call lsp#register_server(
        \ 'name': 'pyls',
        \ 'cmd': server_info->['pyls']},
        \ 'allowlist': ['python'],
        \ })
endif

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
    
    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END


"vim-lsp configuration (coc)

" May need for Vim (not Neovim) since coc.nvim calculates byte offset by count
" utf-8 byte sequence
set encoding=utf-8
" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4s) leads to noticeable
" delays and poor user experience
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


" latex section Latex LATEX


" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" Markdown
" WIMWIKI wimwiki ww

let g:vimwiki_list = [{'path': '~/Documents/vimwiki/',                                                                                                                                  
            \ 'syntax': 'markdown', 'ext': '.md'},                                                                                                                                   
            \ {'level': 1, 'chars': '✗○◐●✓'},
            \ {'level': 2, 'chars': '✗○◐●✓'},
            \ {'level': 3, 'chars': '✗○◐●✓'},     
            \ {'level': 4, 'chars': '✗○◐●✓'},     
            \ {'level': 5, 'chars': '✗○◐●✓'},     
            \ {'level': 6, 'chars': '✗○◐●✓'}]     
     
     let g:vimwiki_listsyms = '✗○◐●✓'
runtime! vimwiki2/autoload/vimwiki/config.vim " Markdown preview
" configs 

" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" for path with space
" valid: `/path/with\ space/xxx`
" invalid: `/path/with\\ space/xxx`
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
" disable_filename: if disable filename header for preview page, default: 0
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0
    \ }

" use a custom markdown style must be absolute path
" like '/Users/username/markdown.css' or expand('~/markdown.css')
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
" like '/Users/username/highlight.css' or expand('~/highlight.css')
let g:mkdp_highlight_css = ''

" use a custom port to start server or empty for random
let g:mkdp_port = ''

" preview page title
" $name} will be replace with the file name
let g:mkdp_page_title = '「$name}」'

" recognized filetypes
" these filetypes will have MarkdownPreview... commands
let g:mkdp_filetypes = ['markdown']

" set default theme (dark or light)
" By default the theme is define according to the preferences of the system
let g:mkdp_theme = 'dark'

" keybindings

" markdow
nmap <C-s> <Plug>MarkdownPreview            "control + s
nmap <M-s> <Plug>MarkdownPreviewStop        "alt + s
nmap <C-p> <Plug>MarkdownPreviewToggle      "control + p

"normal mode 
nmap g$ g9

" visual mode keybindings
vnoremap g9 g$

