# Meeting
eval "$(starship init zsh)"

# Globals

docs=~/Documents

# List files and folders aliases

alias '1=$OLDPWD'
alias autoninja='ninja'

alias l='locate'
alias ll='eza -lha --sort accessed'
alias ld='eza -lD'              # List directory items only folders
alias lda='eza -lDa'            # List directory items only folders
alias lf='ls -pa | grep -v /'   # List directory items except folders
alias lco='ls -1A|wc -l'        # Count items in a folder
alias f='df -h .'               # Free space of current directory
alias dfx='/home/axe/bin/dfx'   # Dfx adressing
alias yt='yt-dlp --write-auto-sub --sub-langs en,es,zh-Hans,tr --embed-subs'   
alias b='mpv --ytdl-format="bestvideo+bestaudio/best" ytdl:'
alias brics='/home/axe/Downloads/bricsad\./bricscad'

# Job-study intense directions aliases
    # currently project directory
    alias w='cd ${docs}/projects/foldersyncronisation'
    alias a='cd ${docs}/courses/rust/rust_examples'
    # currently study directory
    alias s0='cd ${docs}/master/4/6101-introdution-to-analog-electronic-MIT'
    alias s1='cd ${docs}/master/4/6101-introdution-to-analog-electronic-MIT/6101-lecturenotes/components'
    # currently exam directory
    alias e='cd ${docs}/courses/edx/LFS101x'
    # currently code excercise directory
    alias ce='cd ${docs}/master/2.2/Lessons/CSCE155E/codes/4-loop'
    alias ccna='cd ${docs}/courses/ccna/'

    alias icp='cd ${docs}/vimwiki/courses/icp_bootcamp'

    alias d='vim ${docs}/phd/purposal/plantSignalPurposal.tex'
    alias jb='cd ${docs}/jobApplications/windborne'
    alias uc='vim ${docs}/job/editing/ugurcigdem/latex/thesis_main.tex'
    alias ucdir='cd ${docs}/job/editing/ugurcigdem/'


# Functions

    fin() {
        
        # Define ANSI color codes
        GREEN='\033[0;32m'
        RED='\033[0;31m'
        NC='\033[0m'  # No Color
    
        echo -e "Files containing \"$1\":"
        count=1
        while IFS= read -r -d '' file; do
            echo -e "${GREEN} $count. $file ${NC}"
            ((count++))
        done < <(find . -type f -exec grep -q "$1" {} \; -print0)
    
        count=1
        echo -e "Opening files and highlighting:"
        while IFS= read -r -d '' file; do
            echo -e "${RED}$count. \"$file\" ${NC}"
            evince --find="$1" "$file" &
            ((count++))
        done < <(find . -type f -exec grep -q "$1" {} \; -print0)
    }
    # Dictionary query

    dic() {
        cd ${docs}/DIC
    
        # Define ANSI color codes
        GREEN='\033[0;32m'
        RED='\033[0;31m'
        NC='\033[0m'  # No Color
    
        echo -e "Files containing \"$1\":"
        count=1
        while IFS= read -r -d '' file; do
            echo -e "${GREEN} $count. $file ${NC}"
            ((count++))
        done < <(find . -type f -exec grep -q "$1" {} \; -print0)
    
        count=1
        echo -e "Opening files and highlighting:"
        while IFS= read -r -d '' file; do
            echo -e "${RED}$count. \"$file\" ${NC}"
            evince --find="$1" "$file" &
            ((count++))
        done < <(find . -type f -exec grep -q "$1" {} \; -print0)
    }

    # Streak begining date calculator from today with streak days
    streak() {
        days=$1
        streak_start_date=$(date -d "-$days days" "+%Y-%m-%d")
        echo "$streak_start_date"
    }

# Folder jumpping aliases
alias doc='cd ${docs}'
alias prj='cd ${docs}/projects'
alias cour='cd ${docs}/courses'
alias des='cd ~/Desktop'
alias pic='cd ~/Pictures'
alias vid='cd ~/Videos'
alias ch='cd ~/cache'
alias chc='cd ~/cache/code'
alias so='cd ~/cache/so'
alias dow='cd ~/Downloads'
alias cac='cd ~/cache'
alias 32='cd ${docs}/master/3.2/'
alias 31='cd ${docs}/master/3.1/'
alias 4='cd ${docs}/master/4/'
alias t='cd ${docs}/master/3.2/thesis/latex'
alias 4t='cd ${docs}/master/3.2/thesis/latex/gfx/gfx/graphs/4T'
alias v='cd ${docs}/master/3.2/thesis/latex/gfx/gfx/graphs/VI'
alias ti='cd /opt/ti/msp430-gcc-9.3.1.11_linux64/bin'
alias vw='cd ${docs}/vimwiki'
alias agenda='cd ${docs}/AGENDA/2024/'
alias phd='cd ${docs}/phd/'
alias 61='cd ${docs}/vimwiki/courses/6101/'
alias rt='cd ~/cache/russian/Russian-Phd-LaTeX-Dissertation-Template'
alias clipitlocation='cd ~/.local/share/clipit/history'

# System wide calling bin functions 
# PATH="$PATH:$(pwd)"
# export PATH=$PATH:$(/opt/ti/msp430-gcc-9.3.1.11_linux64/bin)

alias report='cd $(ls -td ${docs}/master/3.2/Obsidian/status/Reports/*/ | head -n 1)'
 
# File opening aliases
alias o='xdg-open'
alias c='bc -l'

#Notes
alias vn='vim ~/Desktop/vim\ port' 
alias vg='vim ${docs}/GIT/git_notes.md' 

# config shortcuts
alias vrc='vim ~/.vimrc'
alias brc='vim ~/.bashrc'
alias zrc='vim ~/.zshrc'
alias bal='vim ~/.bash_aliases'
alias vxr='vim ~/.Xdefaults'
alias vi3='vim ~/.config/i3/config'
alias arc='vim ~/.config/awesome/rc.lua' 
alias all='vim ~/.config/alacritty/alacritty.toml' 

alias vr='~/.vimrc'
alias br='~/.bashrc'
alias zr='~/.zshrc'
alias ba='~/.bash_aliases'
alias vx='~/.Xdefaults'
alias v3='~/.i3/config' 
alias al='~/.config/alacritty/alacritty.toml' 

# Invoking aliases 

alias mendeley='QT_AUTO_SCREEN_SCALE_FACTOR=0.1 /usr/bin/mendeleydesktop'

